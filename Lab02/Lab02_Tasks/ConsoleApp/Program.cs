﻿using System;

namespace ConsoleApp
{
    class Program
    {
        private static readonly Guid AndroidCategoryId = Guid.Parse("aa02dc64-5c07-40fe-a916-175165b9b90f");

        static void Main(string[] args)
        {
            EagerLoadProducts();
            PerformServerSideQuery();
            PerformServerSideQueryWithIsInRange();
        }

        /// <summary>
        /// Eagerly load products with its categories
        /// </summary>
        private static void EagerLoadProducts()
        {
            // TODO...
        }

        /// <summary>
        /// Find all android smartphones, within price range 7_000 to 22_000 CZK
        /// and return them ordered by price ascending.
        /// Perform this query entirely on the server side
        /// </summary>
        private static void PerformServerSideQuery()
        {
            // TODO...
        }

        /// <summary>
        /// Same task as in previous method, only this time use IsInRangeExtension 
        /// method for decimal type and see what happens :)
        /// </summary>
        private static void PerformServerSideQueryWithIsInRange()
        {
            // TODO...
        }
    }
}
