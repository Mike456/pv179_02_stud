﻿namespace DemoEshop.DataAccessLayer.EntityFramework
{
    public static class DecimalExtensions
    {
        public static bool IsInRange(this decimal value, decimal min, decimal max)
        {
            return value >= min && value <= max;
        }
    }
}
